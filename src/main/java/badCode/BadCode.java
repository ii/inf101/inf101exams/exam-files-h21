package badCode;

import java.util.ArrayList;
import java.util.Random;

public class BadCode {

	static Random random = new Random();

	/**
	 * This method returns a list of length 6 
	 * containing the numbers from 1 to 6 in random order.
	 * 
	 * @return a random permutation of the numbers from 1 to 6
	 */
	public ArrayList<Integer> roll() {
		
		boolean needMore = false;
		ArrayList<Integer> dices = new ArrayList<Integer>();
		int rolls = 0;
		while(!needMore && rolls<3) {
			int roll = random.nextInt(6);
			if(!dices.contains(roll)) {
				dices.add(roll);
				if(dices.size()==6) {
					needMore=true;
				}
			}
		}
		
		return dices;
	}
	
	public static void main(String[] args) {
		System.out.println(roll());
	}
}

