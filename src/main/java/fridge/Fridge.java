package fridge;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to model a fridge. The fridge contains a list of fridge items. 
 * The fridge can contain max <code>maxItems</code> items.
 * @author Sondre Bolland
 *
 * @param <FridgeItem>
 */
public class Fridge<FridgeItem extends T> {

	/**
	 * Maximum capacity of fridge. 
	 */
	private final int maxItems = 20;
	/**
	 * List of fridge items
	 */
	private List<Object> fridgeItems;
	
	public FridgeWrong() {
		fridgeItems = new ArrayList<>(maxItems);
	}
	
	/**
	 * Place a food item in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param item to be placed
	 * @return true if the item was placed in the fridge, false if not
	 */
	public boolean placeIn(T extends FridgeItem item) {
		if (item == null)
			throw new IllegalArgumentException("Cannot add null to the fridge.");
		if (fridgeItems.size() >= maxItems)
			return false;
		
		return fridgeItems.add(item);
	}
	
	/**
	 * Remove all items from the fridge
	 */
	public void emptyFridge() {
		for (FridgeItem item: fridgeItems) {
			if (item instanceof Object)
				fridgeItems.remove(item);
		}
	}
	
	/**
	 * Place list of food items in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param items to be placed
	 * @return true if all items were placed in the fridge, false if not
	 */
	public void addAll(List<FridgeItem extends T> items) {
		if (items.size()+size() >= maxItems)
			throw new IndexOutOfBoundsException();
		
		for (T item: items) {
			placeIn(item)
		}
	}
	
	/**
	 * Number of fridge items in the fridge
	 * @return number of items in fridge
	 */
	public int size() {
		return fridgeItems.size();
	}
	
	
	 
}
